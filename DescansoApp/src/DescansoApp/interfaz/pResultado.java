package DescansoApp.interfaz;

import javax.swing.JFrame;
import javax.swing.JPanel;
import DescansoApp.dominio.Sistema;

public class pResultado extends javax.swing.JPanel {

    private JFrame ventana;
    private JPanel padre;
    private Sistema modelo;
    private DescansoApp.dominio.Ciudad ciudad;
    
    public pResultado(Sistema unModelo, DescansoApp.dominio.Ciudad unaCiudad, JFrame unaVentana, JPanel unPadre) {
        initComponents();
        
        modelo = unModelo;
        ciudad = unaCiudad;
        ventana = unaVentana;
        padre = unPadre;
        
        lblNombre.setText(ciudad.getNombre());
        //lblDes.setText(ciudad.getDescripcion());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblNombre = new javax.swing.JLabel();
        lblEditarCiudad = new javax.swing.JLabel();

        setOpaque(false);

        lblNombre.setFont(new java.awt.Font("Arial", 0, 15)); // NOI18N
        lblNombre.setForeground(new java.awt.Color(255, 255, 255));
        lblNombre.setText("Nombre Ciudad");
        lblNombre.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        lblNombre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblNombreMouseClicked(evt);
            }
        });

        lblEditarCiudad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/DescansoApp/imagenes/Editar.png"))); // NOI18N
        lblEditarCiudad.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblEditarCiudadMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(lblNombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblEditarCiudad)
                .addGap(301, 301, 301))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblEditarCiudad)
                    .addComponent(lblNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void lblNombreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblNombreMouseClicked
        ventana.remove(padre);
        ventana.add(new pnlInformacionCiudad(modelo, ciudad, ventana));
        ventana.pack();
    }//GEN-LAST:event_lblNombreMouseClicked

    private void lblEditarCiudadMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblEditarCiudadMouseClicked
        // TODO add your handling code here:
        ventana.remove(padre);
        ventana.add(new pnlAgregarCiudad(modelo, ciudad));
        ventana.pack();
    }//GEN-LAST:event_lblEditarCiudadMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblEditarCiudad;
    private javax.swing.JLabel lblNombre;
    // End of variables declaration//GEN-END:variables
}
