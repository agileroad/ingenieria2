package turismoApp.dominio;

import DescansoApp.dominio.Sistema;
import DescansoApp.dominio.Ciudad;
import DescansoApp.dominio.Viaje;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

public class SistemaTest {

    @Test
    public void testGetListaViajes() throws Exception {
        System.out.println("Lista Viajes");
        Viaje viaje= new Viaje();
        viaje.setNombre("viaje");
        Sistema instance = new Sistema();
        instance.agregarViaje(viaje);
        
        /*ArrayList<Viaje> expResult= new ArrayList();
        expResult.add(viaje);*/
        ArrayList<Viaje> result = instance.getListaViajes();
        
        assertFalse(result.isEmpty());
    }

    @Test
    public void testGetListaCiudades() {
        System.out.println("Lista Ciudades");
        Sistema instance = new Sistema();
        Ciudad ciudad= new Ciudad();
        instance.agregarCiudad(ciudad);

        /*ArrayList<Ciudad> expResult = new ArrayList();
        expResult.add(ciudad);*/
        ArrayList<Ciudad> result = instance.getListaCiudades();
        assertFalse(result.isEmpty());
    }
    
    @Test
    public void testAgregarViaje() throws Exception {
        System.out.println("Agregar Viaje(agrega un viaje correctamente)");
        Viaje viaje = new Viaje();
        viaje.setNombre("Viaje");
        Sistema instance = new Sistema();
        instance.agregarViaje(viaje);
        
        /*ArrayList<Viaje> expResult= new ArrayList();
        expResult.add(viaje);*/
        ArrayList<Viaje> result = instance.getListaViajes();
        assertFalse(result.isEmpty());
    }

    @Test(expected = Exception.class)
    public void testAgregarViaje2() throws Exception {
        System.out.println("Agregar Viaje(agrega dos viajes con el mismo nombre)");
        Viaje viaje = new Viaje();
        viaje.setNombre("Viaje");
        Viaje viaje2 = new Viaje();
        viaje2.setNombre("Viaje");
        Sistema instance = new Sistema();
        Exception error= null;
        instance.agregarViaje(viaje);
        
        instance.agregarViaje(viaje2);
    }

    @Test
    public void testEliminarViaje() throws Exception {
        System.out.println("Eliminar Viaje(el viaje existe)");
        Viaje unViaje = new Viaje();
        unViaje.setNombre("Viaje");
        Sistema instance = new Sistema();
        instance.agregarViaje(unViaje);
        
        boolean result = instance.eliminarViaje(unViaje);
        assertTrue(result);
    }
    
    
    @Test
    public void testEliminarViaje2() {
        System.out.println("Eliminar Viaje(el viaje no existe)");
        Viaje unViaje = new Viaje();
        unViaje.setNombre("Viaje");
        Sistema instance = new Sistema();

        
        boolean result = instance.eliminarViaje(unViaje);
        assertFalse(result);
    }

    @Test
    public void testPertenece() {
        System.out.println("Pertenece Viaje(el viaje no pertenece al sistema)");
        Viaje viaje = new Viaje();
        viaje.setNombre("Viaje");
        Sistema instance = new Sistema();
        
        boolean result = instance.pertenece(viaje);
        assertFalse(result);
    }
    
    @Test
    public void testPertenece2() throws Exception {
        System.out.println("Pertenece Viaje 2(el viaje pertenece al sistema)");
        Viaje unViaje = new Viaje();
        unViaje.setNombre("Viaje");
        Sistema instance = new Sistema();
        instance.agregarViaje(unViaje);
        
        boolean result = instance.pertenece(unViaje);
        assertTrue(result);
    }

    
    @Test
    public void testAgregarCiudad() {
        //Unica prueba de agregar ciudad, ya que las ciudades las agregamos nosotros por codigo, en usuario no interactua
        System.out.println("agregarCiudad(agrega ciudad que no existe)");
        Ciudad ciudad = new Ciudad();
        ciudad.setNombre("Fray Bentos");
        Sistema instance = new Sistema();
        instance.agregarCiudad(ciudad);
        
        ArrayList<Ciudad> result = instance.getListaCiudades();
        assertFalse(result.isEmpty());
    }
}